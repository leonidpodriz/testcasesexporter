from django.contrib import admin

from db_exporter.models import DatabaseSetting


@admin.register(DatabaseSetting)
class DatabaseSettingAdmin(admin.ModelAdmin):
    list_display = ("key", "value")
    list_editable = ("value",)
