from django.db import models


class DatabaseSetting(models.Model):
    KEYS = (
        ("server", "Server"),
        ("database", "Database name"),
        ("login", "Login"),
        ("password", "Password"),
    )

    key = models.CharField(max_length=20, choices=KEYS, unique=True)
    value = models.CharField(max_length=255)

    def __str__(self):
        return self.key


