from django.apps import AppConfig


class DbExporterConfig(AppConfig):
    name = 'db_exporter'
