import re
import pyodbc

from datetime import datetime
from typing import Tuple

from django.core.exceptions import ValidationError
from db_exporter.models import DatabaseSetting


class TestManager:
    QUERY_SUITE_NAMES: str = "select distinct SuiteName from DetailedTestCaseResults"
    QUERY_SUITE_NAMES_BY_DATES: str = """
    select distinct SuiteName from DetailedTestCaseResults WHERE DateTime >= '%s' and DateTime <= '%s'
    """
    QUERY_TESTS_BY_SUITE_NAME: str = """
        SELECT TOP (100)
            DateTime,
            SuiteName,
            TestCaseName,
            VersionUnderTest,
            TestResult
        FROM DetailedTestCaseResults
        WHERE SuiteName IN (%s) and DateTime >= '%s' and DateTime <= '%s'
    """

    def __init__(self, driver: str, server: str, database: str, login: str, password: str,
                 trusted_connection: str = 'yes') -> None:
        self.driver = driver
        self.server = server
        self.database = database
        self.login = login
        self.password = password
        self.trusted_connection = trusted_connection

        connection_string = self._get_connection_string()
        connection_string_filtered = connection_string.replace(password, "##########")

    def _get_query_suite_names(self) -> str:
        return self.QUERY_SUITE_NAMES

    def _get_query_suite_names_by_dates(self, start_date: datetime, end_date: datetime) -> str:
        return self.QUERY_SUITE_NAMES_BY_DATES % (start_date, end_date)

    def _get_query_tests_by_suite_name(self, suite_names: Tuple[str], start_date: datetime, end_date: datetime):
        return self.QUERY_TESTS_BY_SUITE_NAME % (
            ", ".join(repr(name) for name in suite_names),
            start_date,
            end_date,
        )

    def _get_connection_string(self) -> str:
        return (
                'DRIVER={%s};'
                'Server=%s;'
                'Database=%s;'
                'UID=%s;'
                'PWD=%s;'
                'Trusted_Connection=%s;' %
                (self.driver, self.server, self.database, self.login, self.password, self.trusted_connection)
        )

    def _run_sql_query(self, sql_query: str) -> tuple:
        connection_string: str = self._get_connection_string()

        with pyodbc.connect(connection_string) as connection:
            cursor = connection.cursor()
            cursor.execute(sql_query)

            return tuple(cursor.fetchall())

    def get_suite_names(self, start_date: datetime, end_date: datetime) -> tuple:
        sql_query: str = self._get_query_suite_names_by_dates(start_date, end_date)

        suite_names: tuple = tuple(row[0] for row in self._run_sql_query(sql_query))

        return suite_names

    def get_suite_names_by_dates(self, start_date: datetime, end_date: datetime) -> tuple:
        sql_query: str = self._get_query_suite_names_by_dates(start_date, end_date)

        suite_names: tuple = tuple(row[0] for row in self._run_sql_query(sql_query))

        return suite_names

    def get_tests_by_suite_names_and_dates(self, suite_names: Tuple[str], start_date: datetime, end_date: datetime):
        sql_query: str = self._get_query_tests_by_suite_name(suite_names, start_date, end_date)

        tests: tuple = self._run_sql_query(sql_query)

        return tests


def get_export_setting(key: str) -> str:
    setting = DatabaseSetting.objects.filter(key=key).first()

    if setting is None:
        raise ValidationError(f"'{key}' was not specified in setting!")

    return setting.value


def get_export_login() -> str:
    return get_export_setting("login")


def get_export_password(particular_hide: bool = True) -> str:
    password: str = get_export_setting("password")

    if not particular_hide:
        return password

    hidden_password = re.sub(r".", "#", password)
    particular_hidden_password = hidden_password[:-3] + password[-3:]

    return particular_hidden_password


def get_export_server() -> str:
    return get_export_setting("server")


def get_export_database() -> str:
    return get_export_setting("database")


def get_suite_names_by_date(date_from: datetime, date_to: datetime) -> Tuple[Tuple[str, str]]:
    login = get_export_login()
    server = get_export_server()
    password = get_export_password()
    database = get_export_database()
    manager = TestManager(
        "SQL Server Native Client 11.0",
        server,
        database,
        login,
        password,
    )

    suite_names: Tuple[str] = manager.get_suite_names_by_dates(date_from, date_to)

    return tuple((name, name) for name in suite_names)


class TestCaseResult:
    def __init__(self, date_time, suite_name, test_case_name, version_under_test, test_result):
        self.date_time = date_time
        self.suite_name = suite_name
        self.test_case_name = test_case_name
        self.version_under_test = version_under_test.split("-")[0] if version_under_test else version_under_test
        self.test_result = test_result


def get_tests_by_suite_names_and_dates(suite_name: str, start_date: datetime, end_date: datetime):
    login = get_export_login()
    server = get_export_server()
    password = get_export_password()
    database = get_export_database()
    manager = TestManager(
        "SQL Server Native Client 11.0",
        server,
        database,
        login,
        password,
    )

    test_cases = manager.get_tests_by_suite_names_and_dates((suite_name,), start_date, end_date)

    return [TestCaseResult(*result) for result in test_cases]
