import django_tables2 as tables


colors_by_result = {
    "Passed": "#28a745",
    "Warning": "#fdcb6e",
    "Error": "#e17055",
    "Failed": "#d63031",
    "Fatal": "#e84393",
}


def get_background_from_value(value):
    color = colors_by_result.get(value, "transparent")
    return f"background: {color}"


class TestResultColumn(tables.Column):

    def render(self, record):
        return str("<a>test</a>")


class TestCasesResultsTable(tables.Table):
    date_time = tables.Column()
    test_case_name = tables.Column()
    version_under_test = tables.Column()
    test_result = tables.Column(attrs={
        "td": {"style": get_background_from_value}
    })
