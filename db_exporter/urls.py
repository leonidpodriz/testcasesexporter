from django.urls import path

from db_exporter.views import select_slot_view, select_suite_name_view, test_cases_result_table

urlpatterns = [
    path("", select_slot_view, name="export"),
    path("select_suite_name/", select_suite_name_view, name="select_suite_name"),
    path("test_cases_result_table/", test_cases_result_table, name="test_cases_result_table"),
]
